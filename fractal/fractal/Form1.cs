﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace fractal
{

    public partial class Form1 : Form
    {
        Pen fractalPen = new Pen(Color.Black, 2);
        Graphics g1;
        Bitmap bm;
        private Point mdown;
        
        public Form1()
        {
            InitializeComponent();

            bm = new Bitmap(640, 480);
            g1 = Graphics.FromImage(bm);
            this.DoubleBuffered = true;
            init();
            start();
        }
        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;
            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public float H
            {
                get { return h; }
            }
            public float S
            {
                get { return s; }
            }
            public float B
            {
                get { return b; }
            }
            public int A
            {
                get { return a; }
            }
            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }
            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;





                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }
        }




        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Refresh();
                using (Graphics g = CreateGraphics())
                {
                    this.DoubleBuffered = true;
                    Rectangle rect = GetRectangle(mdown, e.Location);
                    g.DrawRectangle(Pens.Black, rect);
                }
            }

        }

        //Save button menu bar action class
        //Saves the file as a bitmap from user input into the file stream dialogue box
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Bitmap Image|*.bmp";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.  
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();
                // Saves the fractal bitmap as a bitmap image
                
                bm.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                fs.Close();
            }
        }

        //Load button menu bar item action class
        //Loads an image file, iterates through each pixel to get color from Argb and then outputs into the graphics
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
                openFileDialog1.Title = "Open an Image File";
                openFileDialog1.ShowDialog();
                if (openFileDialog1.FileName != "")
                {
                    // Saves the Image via a FileStream created by the OpenFile method.  
                    System.IO.FileStream fs =
                       (System.IO.FileStream)openFileDialog1.OpenFile();
                    // Saves the fractal bitmap as a bitmap image
                    string fullPath = openFileDialog1.FileName;
                    //string directoryPath = Path.GetDirectoryName(openFileDialog1);
                    bm = (Bitmap)Image.FromFile(fullPath, true);

                    int x, y;

                    // Loop through the images pixels to reset color.
                    for (x = 0; x < bm.Width; x++)
                    {
                        for (y = 0; y < bm.Height; y++)
                        {
                            Color pixelColor = bm.GetPixel(x, y);
                            Color newColor = Color.FromArgb(pixelColor.A, pixelColor.R, pixelColor.G, pixelColor.B);
                            bm.SetPixel(x, y, newColor);
                        }
                    }
                    Invalidate();
                    g1 = Graphics.FromImage(bm);
                    // Set the bitmap to display the image.
                    g1.DrawImageUnscaled(bm, 0, 0);
                    //Refresh the bitmap image to display the new content
                    Refresh();
                    //Close the filestream and dialogue box
                    fs.Close();
                }
            }
            //Catches error if the file is not found and pops up an error message to the user
            catch (System.IO.FileNotFoundException)
            {
                MessageBox.Show("There was an error opening the bitmap." +
                    "Please check the path.");
            }
        }

        //Listener class for the refresh menu tool bar item, re-draws and refreshes the mandelbrot
        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Invalidate();
            mandelbrot();
        }

        private void paletteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var bitmap = new Bitmap(640, 480, PixelFormat.Format8bppIndexed);

            ColorPalette palette = bitmap.Palette;
            palette.Entries[0] = Color.Black;
            int x, y;
            float h, b, alt = 0.0f;
            for (int i = 1; i < 256; i++)
            {
                h = pointcolour(xstart + xzoom * (double)i, ystart + yzoom * (double)i); // color value
                // set to whatever colour here...
                palette.Entries[i] = Color.FromArgb((i * 7) % 256, (i * 7) % 256, 255);
                b = 1.0f - h * h; // brightnes
                Color col = HSBColor.FromHSB(new HSBColor(h * i, 0.8f * i, b * i));
                alt = h;
                fractalPen = new Pen(col);
                g1.DrawLine(fractalPen, i, i, i + 1, i);
            }
            bitmap.Palette = palette;
            g1.DrawImageUnscaled(bitmap,0,0);
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {                   
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                Refresh();
            }
        }
        static public Rectangle GetRectangle(Point p1, Point p2)
        {
            return new Rectangle(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y),
                Math.Abs(p1.X - p2.X), Math.Abs(p1.Y - p2.Y));
        }
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            mdown = e.Location;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //Drawing fractal inside the paint method
            Graphics g1 = e.Graphics;
            g1.DrawImageUnscaled(bm, 0, 0);
        }

        private static float xy;
        private HSBColor HSBcol = new HSBColor();



        public void init() // all instances will be prepared
        {
            HSBcol = new HSBColor();
            finished = false;
            x1 = 640;
            y1 = 480;
            xy = (float)x1 / (float)y1;
            finished = true;
        }
        
        public void destroy() // delete all instances 
        {
            if (finished)
            {
                g1 = null;
                Cursor.Current = Cursors.Default;
                System.GC.SuppressFinalize(this); // garbage collection
            }
        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;
            action = false;
            //Set label 
            toolStripStatusLabel3.Text = "Loading- please wait...";
         
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                                          Color col = HSBColor.FromHSB(new HSBColor(h * 255,0.8f *255,b *255));
                        alt = h;
                        fractalPen = new Pen(col);
                    }
                    g1.DrawLine(fractalPen, x, y, x + 1, y);
                }
            //Set label status to ready and click to zoom
            toolStripStatusLabel3.Text = "Mandelbrot Loaded- Click and drag to zoom";
            Application.DoEvents();
            Cursor.Current = Cursors.WaitCursor;
            action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        /*
        public String getAppletInfo()
        {
            return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
        }
        }
        */
    }
}
